package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private static final Properties properties = new Properties();
	public static final String URL;

	public static synchronized DBManager getInstance()  {
		try {
			return new DBManager();
		} catch (DBException e) {
			e.printStackTrace();
			return null;
		}
	}

	static {
		try {
			properties.load(new FileReader("app.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		URL = (String) properties.get("connection.url");
	}


	private DBManager() throws DBException {
//		try {
//			Class.forName("com.mysql.cj.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			throw new DBException("Cant register jdbc mysql driver", e);
//		}
	}

	public List<User> findAllUsers() throws DBException {
		final List<User> users = new ArrayList<>();
		final String query = "SELECT id, login FROM users order by id";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
				users.add(user);
			}
			return users;
		} catch (SQLException e) {
			throw new DBException("unable to select users", e);
		}
	}

	public boolean insertUser(User user) throws DBException {
		final String query = "INSERT INTO users (login) VALUES (?)";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getInt(1));
				return true;
			}
			return false;
		} catch (SQLException e) {
			throw new DBException("unable to insert user", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		final String query = "DELETE FROM users t WHERE t.login = ?";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			int usersDeleted = 0;
			for (var user: users) {
				statement.setString(1, user.getLogin());
				usersDeleted += statement.executeUpdate();
			}
			return usersDeleted != 0;
		} catch (SQLException e) {
			throw new DBException("unable to delete users", e);
		}
	}

	public User getUser(String login) throws DBException {
		final String query = "SELECT t.id, t.login FROM users t where t.login = ?";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			statement.setString(1, login);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				User user = new User();
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
				return user;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException("unable to select user", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		final String query = "SELECT t.id, t.name FROM teams t where t.name = ?";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			statement.setString(1, name);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
				return team;
			}
			return null;
		} catch (SQLException e) {
			throw new DBException("unable to select team", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		final List<Team> teams = new ArrayList<>();
		final String query = "SELECT id, name FROM teams order by id";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
				teams.add(team);
			}
			return teams;
		} catch (SQLException e) {
			throw new DBException("unable to select teams", e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		final String query = "INSERT INTO teams (name) VALUES (?)";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				team.setId(rs.getInt(1));
				return true;
			}
			return false;
		} catch (SQLException e) {
			throw new DBException("unable to insert team", e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		final String query = "INSERT INTO users_teams\n" +
				"(user_id, team_id)\n" +
				"VALUES (?, ?)";
		Connection con;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DBException("unable to get connection", e);
		}

		try  (PreparedStatement statement = con.prepareStatement(query)) {
			for (var team: teams) {
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				if (statement.executeUpdate() == 0) {
					con.rollback();
					throw new DBException("unable to set teams for user, zero inserted rows", null);
				}
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("unable to set teams for user", e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;	
	}

	public List<Team> getUserTeams(User user) throws DBException {
		final List<Team> teams = new ArrayList<>();
		final String query = "SELECT t.* FROM teams t\n" +
				"left join users_teams ut\n" +
				"on ut.team_id = t.id\n" +
				"left join users u\n" +
				"on u.id = ut.user_id\n" +
				"where u.login = ? order by t.id";
		try (Connection con = DriverManager.getConnection(URL);
			PreparedStatement statement = con.prepareStatement(query)) {
			statement.setString(1, user.getLogin());
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
				teams.add(team);
			}
			return teams;
		} catch (SQLException e) {
			throw new DBException("unable to select users teams", e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		final String query = "DELETE FROM teams t WHERE t.name = ?";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			statement.setString(1, team.getName());
			return statement.executeUpdate() != 0;
		} catch (SQLException e) {
			throw new DBException("unable to delete team", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		final String query = "UPDATE teams t SET t.name = ? WHERE t.id = ?";
		try (Connection con = DriverManager.getConnection(URL);
				PreparedStatement statement = con.prepareStatement(query)) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			return statement.executeUpdate() != 0;
		} catch (SQLException e) {
			throw new DBException("unable to update team", e);
		}
	}
}
